package vvci0170MV.AngajatiApp.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

public class EmployeeTest {
    private Employee e1;
    private Employee e2;
    private Employee e3;
    private Employee e4;

    //se executa automat inaintea fiecarei metode de test
    @Before
    public void setUp() {
        e1 = new Employee();
        e1.setFirstName("Pacuraru");
        e1.setLastName("Ionel");
        e1.setCnp("1234567890876");
        e2 = new Employee();
        e2.setId(1);
        e2.setFirstName("Dumitrescu");
        e2.setLastName("Mihai");
        e2.setCnp("1244567890876");
        e3 = new Employee();
        e4 = null;

        System.out.println("Before test");
    }

    //se executa automat dupa fiecarei metoda de test
    @After
    public void tearDown() {
        e1 = null;
        e2 = null;
        e3 = null;
        e4 = null;
        System.out.println("After test");
    }
    @Test (expected=NullPointerException.class)
    public void testGetFirstNameForEmployee4() {
        assertEquals("Vlad", e4.getFirstName());
    }

    @Test (timeout=10) //asteapta 10 milisecunde
    public void testFictiv(){
        try {
            Thread.sleep (101);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        assertEquals("Vlad", e4.getFirstName());
    }
    @Test
    public void testGetIdForSecondEmployee2() {
        assertEquals(1, e2.getId());
    }

    @Test
    public void testGetFirstNameForEmployee2() {
        assertEquals("Dumitrescu", e2.getFirstName());
    }

    @Test
    public void testSetFirstNameForEmployee2() {
       // assertEquals("Dumitrescu", e2.setFirstName());
    }

    @Test
    public void testConstructor(){
        assertNotEquals("verificam daca s-a creat angajatul 1",e1,null);
    }

    @Test
    public void testGetCnpForSecondEmployee1() {
        assertEquals("1234567890876", e1.getCnp());
    }
}
